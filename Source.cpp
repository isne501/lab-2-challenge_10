#include <iostream>
#include <ctime>
using namespace std;

void Table(); //show table
void UpdateTable(int arr[][7]); //update table
void player(int arr[][7]);//player turn
void AI(int arr[][7]);//bot turn
void CheckWin(int arr[][7], int &check); //check win condition

void main()
{
	int arr[6][7] = { 0 }; //initial table 6x7
	int play = 1; //default player first
	int check = 0; //check win if 0 that mean doesn't win
	int round = 0; //check round play
 
	cout << "This is connect 4 game !!" << endl;
	Table(); //show initial table
	
	cout << "Player first [1] , AI first [2] : "; //player select turn
	cin >> play;

	while (play > 2 || play < 1)
	{
		cout << "Incorrect input, Try again! : ";
		cin >> play;
	}

	while (check==0 && round <= 42 )
	{
		if (play == 1)  //if player 
		{
			player(arr); //player play
			UpdateTable(arr);
			play = 2;
		}
		else if (play == 2)  //if bot 
		{
			AI(arr); //bot play
			UpdateTable(arr);
			play = 1;
		}
		CheckWin(arr, check); //check win
		round++; //round increase
	}
	if (round == 42) //if round = 72 that mean table full -> Draw
	{
		cout << "\nDraw...!!" << endl;
	}
	system("PAUSE");
}


//show initial table
void Table()
{
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "1 | 2 | 3 | 4 | 5 | 6 | 7 |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << endl;
}

//Update table
void UpdateTable(int arr[][7])
{
	cout << "+++++++++++++++++++++++++++" << endl;
	for (int i = 5; i >= 0; i--)
	{
		for (int j = 0; j < 7; j++)
		{ 
			if (arr[i][j] == 0) //if 0 that mean place empty
			{
				cout << " ";
			}
			else if (arr[i][j] == 1) //if 1 that mean player
			{
				cout << "X";
			}
			else if (arr[i][j] == 2) //if 2 that mean bot
			{
				cout << "O";
			}
			if (j < 7)
			{
				cout << " | ";
			}
		}
		cout << endl;
		if (i < 6)
		{
			cout << "+++++++++++++++++++++++++++" << endl;
		}
	}
	cout << "1 | 2 | 3 | 4 | 5 | 6 | 7 |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
}

//player turn
void player(int arr[][7])
{
	int player = 1;
	int row = 0;
	int col;
	int check = 0;

	//prevent incorrect input
	do {
		check = 0;
		cout << "Player turn select column [1-7] : ";
		cin >> col;
		while (col > 7 || col < 1)
		{
			cout << "Incorrect input, Try again! : ";
			cin >> col;
		}
		if (arr[5][col - 1] != 0)
		{
			check = 1;
		}
		cout << "Invalid , Try another column! --> ";
	} while (check == 1);

	//find row
	while (arr[row][col - 1] != 0)
	{
		row++;
	}
	//update table
	arr[row][col - 1] = player;
}

//Bot turn
void AI(int arr[][7])
{
	int Ai = 2;
	int row = 0;
	int col_random;
	int check = 0;

	srand(time(0));
	do
	{
		check = 0;
		col_random = rand() % 7 + 1;

		//Check AI win (diagonal) strategy
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (arr[i + 1][j + 1] == 2 && arr[i + 2][j + 2] == 2 && arr[i + 3][j + 3] == 0 && arr[i][j] == 2)
				{
					if (i == 0 && j == 0)
					{
						col_random = 4;
					}
					else if (i == 1 && j == 1)
					{
						col_random = 5;
					}
					else if (i == 2 && j == 2)
					{
						col_random = 6;
					}
				}
			}
		}
		for (int i = 5; i >= 3; i--)
		{
			for (int j = 0; j < 4; j++)
			{
				if (arr[i - 1][j + 1] == 2 && arr[i - 2][j + 2] == 2 && arr[i - 3][j + 3] == 0 && arr[i][j] == 2)
				{
					if (i == 5 && j == 0)
					{
						col_random = 4;
					}
					else if (i == 4 && j == 1)
					{
						col_random = 5;
					}
					else if (i == 3 && j == 2)
					{
						col_random = 6;
					}
				}
			}
		}
		//Check AI win (vertical) strategy
		for (int j = 0; j < 7; j++)
		{
			for (int i = 0; i < 3; i++)
			{
				if (arr[i + 1][j] == 2 && arr[i + 2][j] == 2 && arr[i + 3][j] == 0 && arr[i][j] == 2)
				{
					if (j == 0)
					{
						col_random = 1;
					}
					else if (j == 1)
					{
						col_random = 2;
					}
					else if (j == 2)
					{
						col_random = 3;
					}
					else if (j == 3)
					{
						col_random = 4;
					}
					else if (j == 4)
					{
						col_random = 5;
					}
					else if (j == 5)
					{
						col_random = 6;
					}
					else if (j == 6)
					{
						col_random = 7;
					}
				}
			}
		}

		//Check AI win horizontal strategy
		for (int i = 0; i < 6; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (arr[i][j + 1] == 2 && arr[i][j + 2] == 2 && arr[i][j + 3] == 0 && arr[i][j] == 2)
				{
					if (j == 0)
					{
						col_random = 4;
					}
					else if (j == 1)
					{
						col_random = 5;
					}
					else if (j == 2)
					{
						col_random = 6;
					}
					else if (j == 3)
					{
						col_random = 7;
					}
				}
				else if (arr[i][j + 1] == 2 && arr[i][j + 2] == 2 && arr[i][j + 3] == 2 && arr[i][j] == 0)
				{
					if (j == 6)
					{
						col_random = 4;
					}
					else if (j == 5)
					{
						col_random = 3;
					}
					else if (j == 4)
					{
						col_random = 2;
					}
					else if (j == 3)
					{
						col_random = 1;
					}
				}
				else if (arr[i][j + 1] == 0 && arr[i][j + 2] == 2 && arr[i][j + 3] == 2 && arr[i][j] == 2)
				{
					if (j == 0)
					{
						col_random = 2;
					}
					else if (j == 1)
					{
						col_random = 3;
					}
					else if (j == 2)
					{
						col_random = 4;
					}
					else if (j == 3)
					{
						col_random = 5;
					}
				}
				else if (arr[i][j + 1] == 2 && arr[i][j + 2] == 0 && arr[i][j + 3] == 2 && arr[i][j] == 2)
				{
					if (j == 0)
					{
						col_random = 3;
					}
					else if (j == 1)
					{
						col_random = 4;
					}
					else if (j == 2)
					{
						col_random = 5;
					}
					else if (j == 3)
					{
						col_random = 6;
					}
				}
			}
		}


		//AI strategy (diagonal) strategy
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (arr[i + 1][j + 1] == 1 && arr[i + 2][j + 2] == 1 && arr[i + 3][j + 3] == 0 && arr[i][j] == 1)
				{
					if (i == 0 && j == 0)
					{
						col_random = 4;
					}
					else if (i == 1 && j == 1)
					{
						col_random = 5;
					}
					else if (i == 2 && j == 2)
					{
						col_random = 6;
					}
				}
			}
		}
		for (int i = 5; i >= 3; i--)
		{
			for (int j = 0; j < 4; j++)
			{
				if (arr[i - 1][j + 1] == 1 && arr[i - 2][j + 2] == 1 && arr[i - 3][j + 3] == 0 && arr[i][j] == 1)
				{
					if (i == 5 && j == 0)
					{
						col_random = 4;
					}
					else if (i == 4 && j == 1)
					{
						col_random = 5;
					}
					else if (i == 3 && j == 2)
					{
						col_random = 6;
					}
				}
			}
		}
		//AI strategy (vertical) strategy
		for (int j = 0; j < 7; j++)
		{
			for (int i = 0; i < 3; i++)
			{
				if (arr[i + 1][j] == 1 && arr[i + 2][j] == 1 && arr[i + 3][j] == 0 && arr[i][j] == 1)
				{
					if (j == 0)
					{
						col_random = 1;
					}
					else if (j == 1)
					{
						col_random = 2;
					}
					else if (j == 2)
					{
						col_random = 3;
					}
					else if (j == 3)
					{
						col_random = 4;
					}
					else if (j == 4)
					{
						col_random = 5;
					}
					else if (j == 5)
					{
						col_random = 6;
					}
					else if (j == 6)
					{
						col_random = 7;
					}
				}
			}
		}

		//AI strategy (horizontal) strategy
		for (int i = 0; i < 6; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (arr[i][j + 1] == 1 && arr[i][j + 2] == 1 && arr[i][j + 3] == 0 && arr[i][j] == 1)
				{
					if (j == 0)
					{
						col_random = 4;
					}
					else if (j == 1)
					{
						col_random = 5;
					}
					else if (j == 2)
					{
						col_random = 6;
					}
					else if (j == 3)
					{
						col_random = 7;
					}
				}
				else if (arr[i][j + 1] == 1 && arr[i][j + 2] == 1 && arr[i][j + 3] == 1 && arr[i][j] == 0)
				{
					if (j == 6)
					{
						col_random = 4;
					}
					else if (j == 5)
					{
						col_random = 3;
					}
					else if (j == 4)
					{
						col_random = 2;
					}
					else if (j == 3)
					{
						col_random = 1;
					}
				}
				else if (arr[i][j + 1] == 0 && arr[i][j + 2] == 1 && arr[i][j + 3] == 1 && arr[i][j] == 1)
				{
					if (j == 0)
					{
						col_random = 2;
					}
					else if (j == 1)
					{
						col_random = 3;
					}
					else if (j == 2)
					{
						col_random = 4;
					}
					else if (j == 3)
					{
						col_random = 5;
					}
				}
				else if (arr[i][j + 1] == 1 && arr[i][j + 2] == 0 && arr[i][j + 3] == 1 && arr[i][j] == 1)
				{
					if (j == 0)
					{
						col_random = 3;
					}
					else if (j == 1)
					{
						col_random = 4;
					}
					else if (j == 2)
					{
						col_random = 5;
					}
					else if (j == 3)
					{
						col_random = 6;
					}
				}
			}
		}

		if (arr[5][col_random - 1] != 0)
		{
			check = 1;
		}
		cout << "AI Select : " << col_random << endl;

	} while (check == 1);

	//find row
	while (arr[row][col_random - 1] != 0)
	{
		row++;
	}
	//Update table
	arr[row][col_random - 1] = Ai;
}

//check win condition
void CheckWin(int arr[][7], int &check)
{
	//win check diagnal (Down)
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (arr[i + 1][j + 1] == 1 && arr[i + 2][j + 2] == 1 && arr[i + 3][j + 3] == 1 && arr[i][j] == 1)
			{
				check = 1;
			}
			else if (arr[i + 1][j + 1] == 2 && arr[i + 2][j + 2] == 2 && arr[i + 3][j + 3] == 2 && arr[i][j] == 2)
			{
				check = 2;
			}
		}
	}

	//win check diagnal (Up)
	for (int i = 5; i >= 3; i--)
	{
		for (int j = 0; j < 4; j++)
		{
			if (arr[i - 1][j + 1] == 1 && arr[i - 2][j + 2] == 1 && arr[i - 3][j + 3] == 1 && arr[i][j] == 1)
			{
				check = 1;
			}
			else if (arr[i - 1][j + 1] == 2 && arr[i - 2][j + 2] == 2 && arr[i - 3][j + 3] == 2 && arr[i][j] == 2)
			{
				check = 2;
			}
		}
	}
	//win check vertical
	for (int j = 0; j < 7; j++)
	{
		for (int i = 0; i < 3; i++)
		{
			if (arr[i + 1][j] == 1 && arr[i + 2][j] == 1 && arr[i + 3][j] == 1 && arr[i][j] == 1)
			{
				check = 1;
			}
			else if (arr[i + 1][j] == 2 && arr[i + 2][j] == 2 && arr[i + 3][j] == 2 && arr[i][j] == 2)
			{
				check = 2;
			}
		}
	}
	//win check horizontal
	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (arr[i][j + 1] == 1 && arr[i][j + 2] == 1 && arr[i][j + 3] == 1 && arr[i][j] == 1)
			{
				check = 1;
			}
			else if (arr[i][j + 1] == 2 && arr[i][j + 2] == 2 && arr[i][j + 3] == 2 && arr[i][j] == 2)
			{
				check = 2;
			}
		}
	}

	if (check == 1)//if check = 1 that mean player win
	{
		cout << "\nYou win...! --> AI said 'Maybe I don't concentrate the game'" << endl;
	}
	else if (check == 2) //if ceck = 2 that mean bot win
	{
		cout << "\nBot Win!! --> AI said 'You are the weekness that I have ever seen!" << endl;
	}
}